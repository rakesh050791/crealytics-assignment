require './lib/multiplication_table.rb'

DEFAULT_SIZE = 10

choice = nil

until choice == "N"
  puts "Lets Multiply!"
  puts "Please enter an integer for table size."

  table_size = gets.chomp
  table_size = table_size.empty? ? DEFAULT_SIZE : table_size
  @multiplication_table = MultiplicationTable.new(table_size)

  @multiplication_table.show_table

  puts 'Run again? Y or N?'

  choice = gets.strip.upcase
end

puts 'Now exiting. Thanks!'
