module InputValidation
  def valid?
    @table_size = @table_size.to_i
    @errors = []

    #Invalid if expression length is greater than 100
    @errors << 'Input is not a valid number. Please enter integer input above 1' if @table_size == 0

    @errors.empty?
  end
end