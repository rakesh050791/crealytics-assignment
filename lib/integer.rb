class Integer
  def prime?
    return true if self < 4
    return false if self%2 == 0
    (3..self/2).step(2).none?{|i| self % i == 0}
  end
end