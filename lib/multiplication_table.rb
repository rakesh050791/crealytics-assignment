require_relative "../lib/input_validation"
require_relative "../lib/integer"

class MultiplicationTable
  include InputValidation
  attr_accessor :table_size, :errors

  def initialize(table_size)
    @table_size = table_size
  end


  def evaluate_input
    if valid?
      generate_table
    else
      puts "Input provided is invalid due to following errors, can't evaluate"
      puts @errors.inspect
    end
  end

  def generate_table
    _rows = first_n_prime(table_size) #array
    _columns = _rows

    _num_of_space = 3

    print " %#{_num_of_space}s " % ' '
    _columns.each {|column_num| print " %-#{_num_of_space}d " % column_num}
    print "\n\n"

    _rows.each do |row_num|
      print "%-#{_num_of_space}d| " % row_num
      _columns.each {|column_num| print " %-#{_num_of_space}d " % (column_num * row_num)}
      print "\n\n"
    end
  end

  def first_n_prime(num)
    primes_array = [1,2,3,5,7,11]
    return primes_array.first(num) if num <= 6
    start_num = primes_array.last + 2

    until primes_array.size == num
      primes_array << start_num if start_num.prime?
      start_num += 2
    end

    primes_array
  end

  def show_table
    evaluate_input
  end

end