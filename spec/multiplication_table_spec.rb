require_relative '../lib/multiplication_table'
require_relative '../lib/input_validation'

describe MultiplicationTable do
  before do
    @multiplication_table = MultiplicationTable.new(4)
  end

  context "#generate_table" do

    context 'testing the number 4' do
      it 'prints a 4 x 4 table' do
        expect {@multiplication_table.generate_table}.to output("      1    2    3    5   \n\n1  |  1    2    3    5   \n\n2  |  2    4    6    10  \n\n3  |  3    6    9    15  \n\n5  |  5    10   15   25  \n\n").to_stdout
      end
    end
  end

  context "#first_n_prime" do
    it 'return first 5 prime numbers' do
      expect(@multiplication_table.first_n_prime(5)).to eql([1, 2, 3, 5, 7])
    end

    it 'return first 10 prime numbers' do
      expect(@multiplication_table.first_n_prime(10)).to eql([1, 2, 3, 5, 7, 11, 13, 17, 19, 23])
    end
  end

  context "#evaluate_input" do
    it 'should throw error when invalid input' do
      @multiplication_table = MultiplicationTable.new(0)
      expect(@multiplication_table).to receive(:generate_table).exactly(0).times
      expect(@multiplication_table.valid?).to eql(false)
      expect(@multiplication_table.errors).to eq(["Input is not a valid number. Please enter integer input above 1"])
      @multiplication_table.evaluate_input
    end

    it 'should evaluate input when valid input' do
      @multiplication_table = MultiplicationTable.new(2)
      expect(@multiplication_table).to receive(:generate_table).exactly(1).times
      expect(@multiplication_table.valid?).to eql(true)
      expect(@multiplication_table.errors).to eq([])
      @multiplication_table.evaluate_input
    end
  end
end