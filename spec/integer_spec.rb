require_relative '../lib/integer'


describe Integer do
  context "#prime?" do

    context 'testing the number 5' do
      it "returns true" do
        expect(5.prime?).to eql(true)
      end
    end

    context 'testing the number 10' do
      it "returns false" do
        expect(10.prime?).to eql(false)
      end
    end

  end
end