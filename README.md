#Write a program that prints out a multiplication table of the first N prime numbers.

* The program must run from the command line and print a table to STDOUT.
* The first row and column of the table should have the N primes, with each cell containing the product of the primes for the corresponding row and column.
* Allow the user to specify different table sizes through a command line option. If the option is not used, the table should contain the first 10 primes by default.

## Installation Instructions

You will require ruby 2.4.1 installed to use this program.

Clone the application from bitbucket, cd into the directory and install all dependencies. 

```
$ git clone git@bitbucket.org:rakesh050791/crealytics-assignment.git
$ cd crealytics
$ bundle install
```

## Usage Instructions

while in the project root directory.

To Run : call main.rb file : 
```
$ ruby main.rb
```

## Running Tests

To run the test suite, run rspec from the root directory.
```
$ rspec
```

Below steps we are executing inside different file's.

```ruby
#Main.rb
#This will import required classes to run the program
require 'lib/multiplication_table'
#Next step is we are asking user to input table size.

#MultiplicationTable.rb
require_relative "../lib/input_validation"
require_relative "../lib/integer"

 ## After getting user's input we are calling MultiplicationTable class for processing the input.
 ## Inside MultiplicationTable class we are calculating first n prime number's as per user's input and printing multiplication table from the first n prime no's result.

#Integer.rb
 ## There is one more class i.e. Integer where we are checking given no from first n prime number's is prime or not. 

#InputValidation.rb 
 ## One last class is InputValidation where we are just validating user's input (it should be + integer greater than 0 ) 
```


